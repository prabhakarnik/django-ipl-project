from django.urls import path
from django.views.decorators.cache import cache_page
from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from . import views

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)

app_name = 'ipl'

urlpatterns = [
    path('/',  views.IndexView.as_view(), name='index'),
    path('/assignment1', cache_page(CACHE_TTL)(views.AssignmentOneView.as_view()), name='assignment1'),
    path('/assignment2', cache_page(CACHE_TTL)(views.AssignmentTwoView.as_view()), name='assignment2'),
    path('/assignment3', cache_page(CACHE_TTL)(views.AssignmentThreeView.as_view()), name='assignment3'),
    path('/assignment4', cache_page(CACHE_TTL)(views.AssignmentFourView.as_view()), name='assignment4'),
    path('/assignment5', cache_page(CACHE_TTL)(views.AssignmentFiveView.as_view()), name='assignment5'),
    path('/assignment1/plot', cache_page(CACHE_TTL)(views.AssignmentOnePlotView.as_view()), name='assignment1plot'),
    path('/assignment2/plot', cache_page(CACHE_TTL)(views.assignment2plot), name='assignment2plot'),
    path('/assignment3/plot', cache_page(CACHE_TTL)(views.AssignmentThreePlotView.as_view()), name='assignment3plot'),
    path('/assignment4/plot', cache_page(CACHE_TTL)(views.AssignmentFourPlotView.as_view()), name='assignment4plot'),
    path('/assignment5/plot', cache_page(CACHE_TTL)(views.AssignmentFivePlotView.as_view()), name='assignment5plot'),

]
