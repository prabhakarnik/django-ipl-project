from django.core.management import BaseCommand
from ipl.models import Matches, Deliveries
from django.db import transaction
import csv


def import_csv(path, model):

    if model == "Matches":
        import_mathces(path)
    else:
        import_deliveries(path)

@transaction.atomic
def import_mathces(path):
    all_rows = []
    with open(path) as f:
        file_reader = csv.reader(f)
        for row in file_reader:
            if row[0] == 'id':
                continue
            all_rows.append(Matches(*row))

    Matches.objects.bulk_create(all_rows)

@transaction.atomic
def import_deliveries(path):
    all_rows = []
    i = 1
    with open(path) as f:
        file_reader = csv.reader(f)
        for row in file_reader:
            if row[0] == 'match_id':
                continue
            row.insert(0, i)
            i = i + 1
            all_rows.append(Deliveries(*row))

    Deliveries.objects.bulk_create(all_rows)


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('path', type=str)
        parser.add_argument('model', type=str)

    def handle(self, *args, **options):
        path = options.get('path', None)
        model = options.get('model', None)
        import_csv(path, model)
