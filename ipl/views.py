from django.shortcuts import render
from django.http import HttpResponse
from django.views import generic
from ipl.models import Matches, Deliveries
from django.db.models import Count, Sum, Avg


class IndexView(generic.TemplateView):

    template_name = 'ipl/index.html'


class AssignmentOneView(generic.ListView):

    template_name = 'ipl/assignment1.html'
    context_object_name = 'season_list'

    
    def get_queryset(self):
        return Matches.objects.values('season').annotate(Count('season')).order_by()


class AssignmentTwoView(generic.ListView):

    template_name = 'ipl/assignment2.html'
    context_object_name = 'winner_season_list'

    
    def get_queryset(self):
        return Matches.objects.values('winner', 'season').annotate(Count('winner')).order_by()


class AssignmentThreeView(generic.ListView):

    template_name = 'ipl/assignment3.html'
    context_object_name = 'extra_runs_conceded'

    def get_queryset(self):
        return Deliveries.objects.values('bowling_team').annotate(Sum('extra_runs')).filter(match_id__in=Matches.objects.values('id').filter(season=2016))


class AssignmentFourView(generic.ListView):

    template_name = 'ipl/assignment4.html'
    context_object_name = 'economical_bowlers'

    
    def get_queryset(self):
        return Deliveries.objects.values('bowler').annotate(economy=Avg('total_runs') * 6).filter(match_id__in=Matches.objects.values('id').filter(season=2015)).order_by('economy')[:10]


class AssignmentFiveView(generic.ListView):

    template_name = 'ipl/assignment5.html'
    context_object_name = 'runrates'

    def runrate(self, team):
        s = 0
        for overs in team:
            s += overs['total_runs__sum']
            overs['total_runs__sum'] = s

        for overs in team:
            overs['total_runs__sum'] = overs['total_runs__sum'] / overs['over']

        return team

    
    def get_queryset(self):
        rr = Deliveries.objects.values('over').annotate(Sum('total_runs')).filter(
            match_id=513, batting_team='Rajasthan Royals')
        mi = Deliveries.objects.values('over').annotate(
            Sum('total_runs')).filter(match_id=513, batting_team='Mumbai Indians')

        return (self.runrate(rr), self.runrate(mi))


class AssignmentOnePlotView(generic.ListView):
    template_name = 'ipl/assignment1plot.html'
    context_object_name = 'season_list'

    
    def get_queryset(self):
        return AssignmentOneView().get_queryset()


def assignment2plot(request):
    winners = AssignmentTwoView().get_queryset()
    winners_per_season = {}
    for rows in winners:
        if rows['winner'] != '':
            if rows['winner'] not in winners_per_season.keys():
                winners_per_season[rows['winner']] = {rows['season']:rows['winner__count']}
            else:
                winners_per_season[rows['winner']][rows['season']] = rows['winner__count']

    season_list = [season for season in range(2008, 2018)]
    for team, wins in winners_per_season.items():
        for season in season_list:
            if season not in wins.keys():
                wins[season] = 0

        sorted_wins = sorted(wins)
        sorted_win_dict = {}
        for win in sorted_wins:
            sorted_win_dict[win] = wins[win]
        winners_per_season[team] = sorted_win_dict

    return render(request, 'ipl/assignment2plot.html', {'winners_per_season': winners_per_season})



class AssignmentThreePlotView(generic.ListView):
    template_name = 'ipl/assignment3plot.html'
    context_object_name = 'extra_runs_conceded'

    
    def get_queryset(self):
        return AssignmentThreeView().get_queryset()


class AssignmentFourPlotView(generic.ListView):
    template_name = 'ipl/assignment4plot.html'
    context_object_name = 'economical_bowlers'

    
    def get_queryset(self):
        return AssignmentFourView().get_queryset()


class AssignmentFivePlotView(generic.ListView):
    template_name = 'ipl/assignment5plot.html'
    context_object_name = 'runrates'

    
    def get_queryset(self):
        return AssignmentFiveView().get_queryset()
